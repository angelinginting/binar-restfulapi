const express = require('express');
const morgan = require('morgan')
const path = require('path');
const api = require('./routes/api.js')
const app = express()
const port = 3000

app.use('/api', api)
app.set('view engine','ejs')
app.use(morgan('dev'))
app.use(express.static(__dirname + '/public'))


app.get("/games", (req, res) => {
    res.render("index", {
      content: "./pages/game",
    })
});

app.get('/', function(req,res){
    res.render(path.join(__dirname, './views/index'),{
        content: './pages/menu'
    })
})

app.listen(port,() => console.log("Server Berhasil Dijalankan"))