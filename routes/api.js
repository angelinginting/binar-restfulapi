const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const Games = require('../controllers/games')
const games = new Games

const logger = (req,res,next) => {
    console.log(`LOG: ${req.method} ${req.url}`);
    next()
}
const api = express.Router()

api.use(logger)
api.use(jsonParser)
app.set('view engine','ejs')

api.get('/games', games.getGames)
api.post('/games', games.getDetailGames)
api.post('/games',jsonParser, games.insertGames)
api.put('/games/:index',jsonParser, games.insertGames)
api.delete('/games/:index',jsonParser, games.deleteGames)

module.exports = api