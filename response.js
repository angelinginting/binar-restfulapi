const message = { 
    '200' : 'sukses',
    '201' : 'data berhasil disimpan',
    '400' : 'gagal diproses'
}

function successResponse(
    res,
    code,
    data,
    meta = {}
    
){
    res.status(code).json({
        data:data,
        meta:{
            code:200,
            message: message[code.toString()],
            ...meta
        }
    })
}

module.exports = {
    successResponse
}